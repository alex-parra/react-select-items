import { useReducer } from "react";

function useSelectedItems(initial = []) {
  const ACTION_SELECT = "ACTION_SELECT";
  const ACTION_UNSELECT = "ACTION_UNSELECT";

  const actions = {
    select: payload => ({ type: ACTION_SELECT, payload }),
    unselect: payload => ({ type: ACTION_UNSELECT, payload })
  };

  const reducer = (state, { type, payload }) => {
    if (type === ACTION_SELECT) {
      return state.map(item => {
        if (item.name !== payload.name) return item;
        return { ...item, isSelected: true };
      });
    }

    if (type === ACTION_UNSELECT) {
      return state.map(item => {
        if (item.name !== payload.name) return item;
        return { ...item, isSelected: false };
      });
    }

    return state;
  };

  const initialItems = initial.map(item => ({ ...item, isSelected: false }));
  const [itemsList, dispatch] = useReducer(reducer, initialItems);

  return [itemsList, dispatch, actions];
}

export default useSelectedItems;
