import React from "react";

const SelectedItems = ({ items }) => {
  const selectedItems = items.filter(item => item.isSelected);

  const noneSelected = () => {
    return (
      <div className="SelectedItems__empty">
        You haven't selected any items yet.
      </div>
    );
  };

  return (
    <header className="SelectedItems">
      <h2 className="SelectedItems__title">Items selected:</h2>
      <div className="SelectedItems__wrap">
        {selectedItems.length === 0 && noneSelected()}
        {selectedItems.map(item => (
          <span key={item.name} className="SelectedItems__item">
            {item.name}
          </span>
        ))}
      </div>
    </header>
  );
};

export default SelectedItems;
