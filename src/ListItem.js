import React from "react";

const ListItem = ({ item, onClick }) => {
  return (
    <li
      onClick={() => onClick(item)}
      className={`List__item List__item--${item.color}`}
      data-selected={item.isSelected}
    >
      <span>{item.name}</span>
    </li>
  );
};

export default React.memo(ListItem, (prev, next) => prev.item === next.item);
